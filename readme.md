# Laravel Resources

__Last Updated: 9/24/2015 by Ian Olson__

## Laravel Resources

- Laravel and Client-Side Boilerplate (Coming Soon)
- List of Go-To Composer Packages (Coming Soon)

## Laravel Quick Start Guide

Start using Laravel 5.1 in a couple of minutes. Follow the easy-to-follow steps and get you up and running with a project in no time.

- [http://laravel.com/docs/5.1/installation](http://laravel.com/docs/5.1/installation)

## Laravel Documentation

- [http://laravel.com/docs/5.1](http://laravel.com/docs/5.1)

## LaraCasts & LaraCasts Forums

It truly is the _Netflix for Developers_, this goes over some great resources in PHP itself, with a focus on Laravel. This is a paid service, but also comes out with great free series from time-to-time.

- [https://laracasts.com/](https://laracasts.com/)
- [https://laracasts.com/discuss](https://laracasts.com/discuss)

## Matt Stauffer Blog

A great blog by matt Stauffer that goes over Laravel and the different ways of using it, also general development tools and workflow. Host on Laravel Podcast.

- [https://mattstauffer.co/blog](https://mattstauffer.co/blog)

## /r/laravel

One of the great resources out there from a great community. Reddit is one of those hit/miss type deals, but with this community, I've found endless help here, as well as provided feedback to multiple developers.

- [https://www.reddit.com/r/laravel](https://www.reddit.com/r/laravel)

## Slack

A great resource of Laravel/PHP developers in the Slack channel. If you haven't used Slack I would say you are missing out on a great tool for resources.

- [https://larachat.co/](https://larachat.co/)

## Nerdery's Laravel Best Practices

Coming soon.